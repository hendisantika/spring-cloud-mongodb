package com.hendisantika.springcloudmongodb.repository;

import com.hendisantika.springcloudmongodb.domain.Employee;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-cloud-mongodb
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-01
 * Time: 08:48
 * To change this template use File | Settings | File Templates.
 */
public interface EmployeeRepository extends MongoRepository<Employee, String> {

}