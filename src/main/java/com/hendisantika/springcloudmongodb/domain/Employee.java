package com.hendisantika.springcloudmongodb.domain;

import org.springframework.data.annotation.Id;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-cloud-mongodb
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-01
 * Time: 08:47
 * To change this template use File | Settings | File Templates.
 */
public class Employee {
    @Id
    private volatile String id;

    private String employeeName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public Employee(String id, String employeeName) {
        this.id = id;
        this.employeeName = employeeName;
    }

    @Override
    public String toString() {
        return "Employee [id=" + id + ", employeeName=" + employeeName + "]";
    }
}
